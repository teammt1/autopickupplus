package teammt.autopickupplus.pickup;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;

public class PickupManager extends Registerable {

    public PickupManager(MLib lib) {
        super(lib);
    }

    public boolean isAutopickupEnabled() {
        return lib.getConfigurationAPI().getConfig().getBoolean("autopickup");
    }

    public boolean isApplyFortuneEnabled() {
        return lib.getConfigurationAPI().getConfig().getBoolean("apply-fortune");
    }

    public boolean isPickupExpEnabled() {
        return lib.getConfigurationAPI().getConfig().getBoolean("pickup-exp");
    }

    public List<String> getPickupWorlds() {
        return lib.getConfigurationAPI().getConfig().getStringList("autopickup-worlds");
    }

    public List<String> getExpWorlds() {
        return lib.getConfigurationAPI().getConfig().getStringList("autoxp-worlds");
    }

    @SuppressWarnings("deprecation")
    public List<ItemStack> calculateDrops(Block block, Player player) {
        List<ItemStack> drops = new ArrayList<ItemStack>();

        Material type = block.getType();
        String value = lib.getConfigurationAPI().getConfig().getString("blocks." + type.name() + ".drops");
        if (value == null)
            return new ArrayList<>();

        Material drop = Material.matchMaterial(value);
        boolean fortune = lib.getConfigurationAPI().getConfig().getBoolean("blocks." + type.name() + ".fortune");

        if (drop == null)
            return new ArrayList<ItemStack>();

        if (fortune) {
            int fortuneLevel = player.getInventory().getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
            int amount = 1 + ThreadLocalRandom.current().nextInt(2 + fortuneLevel);
            if (fortuneLevel == 0)
                amount = 1;
            drops.add(new ItemStack(drop, amount));
        } else {
            drops.add(new ItemStack(drop));
        }

        return drops;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!isAutopickupEnabled())
            return;

        World world = event.getBlock().getWorld();
        if (!getPickupWorlds().contains(world.getName()))
            return;

        List<ItemStack> drops = calculateDrops(event.getBlock(), event.getPlayer());
        if (drops.size() != 0) {
            if (lib.getCompatibilityApi().getServerVersion().getMajor() >= 12)
                event.setDropItems(false);
            else
                event.getBlock().setType(Material.AIR);
        }

        for (ItemStack drop : drops)
            event.getPlayer().getInventory().addItem(drop);

        int xp = event.getExpToDrop();
        if (xp != 0 && isPickupExpEnabled() && getExpWorlds().contains(world.getName())) {
            event.setExpToDrop(0);
            event.getPlayer().giveExp(xp);
        }
    }

}
