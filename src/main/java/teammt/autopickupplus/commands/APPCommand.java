package teammt.autopickupplus.commands;

import org.bukkit.command.CommandSender;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;

@RegisterableInfo(command = "autopickupplus")
public class APPCommand extends Registerable {

    public APPCommand(MLib lib) {
        super(lib);
    }

    @SubcommandInfo(subcommand = "", permission = "teammt.autopickupplus.help")
    public void handleHelp(CommandSender sender) {
        lib.getMessagesAPI().sendMessage("help-menu", sender);
    }

    @SubcommandInfo(subcommand = "help", permission = "teammt.autopickupplus.help")
    public void handleHelpWithArgs(CommandSender sender) {
        lib.getMessagesAPI().sendMessage("help-menu", sender);
    }

    @SubcommandInfo(subcommand = "reload", permission = "teammt.autopickupplus.reload")
    public void handleReload(CommandSender sender) {
        try {
            lib.getConfigurationAPI().reloadAll();
            lib.getMessagesAPI().reloadSharedConfig();

            lib.getMessagesAPI().sendMessage("reload-success", sender);
        } catch (Exception e) {
            lib.getMessagesAPI().sendMessage("reload-failed", sender);
        }
    }

}
