package teammt.autopickupplus.main;

import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.autopickupplus.commands.APPCommand;
import teammt.autopickupplus.pickup.PickupManager;

public class AutoPickupPlus extends JavaPlugin {
    private MLib lib;

    @Override
    public void onEnable() {
        this.lib = new MLib(this);
        lib.getConfigurationAPI().requireAll();

        new PickupManager(lib).register();

        new APPCommand(lib).register();
    }
}
